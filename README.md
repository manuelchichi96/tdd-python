# TDD con Lenguaje de programacion Python.

Este repositorio contiene la resolucion al problema de graficar numeros al formato solicitado de la capacitacion WFC aplicando desarrollo guiado por pruebas en el lenguaje de programacion Python. 

## Resolucion problema.
Se ha creado una clase SevenDisplay, en la cual se guarda un diccionario con todos los digitos como llave y como valor se tiene un vector de 3 cadenas de caracteres donde cada posicion del arreglo representa la altura (0 arriba, 1 medio y 2 bajo). El valor corresponde a la cadena que se imprime en pantalla en dicha altura. Luego al iniciar el algoritmo lo que se realiza es crear un arreglo con 3 cadenas, donde cada cadena tiene un salto de linea al final. La idea es agregar antes del salto de linea el valor del vector de digitos. Como paso siguiente se recorre cada cifra del numero. Por cada cifra, se buscara en el diccionario el vector de dicha cifra. Despues se recorre el vector de la cifra encontrada y se ira actualizando el vector resultado al agregar los valores de dich altura. Finalmente se concatena los 3 niveles y se imprime la cadena correctamente.

## Ejecucion.

### Requisitos previos.

[Python](https://www.python.org/downloads/) para crear el entorno virtual y ejecutar los archivos.


### Comandos.

Para realizar la ejecucion de las pruebas de los scripts que solucionan los problemas se deben ejecutar los siguientes comandos.

```
python3 -m venv env
source env/bin/activate
pip3 install -r requeriments.txt
python3 -m pytest tests/
```
