class SevenDisplay():

   DIGITS = {
        '1':[' ','|','|'],
        '2':[' _ ',' _|','|_ '],
        '3':[' _ ',' _|',' _|'],
        '4':['   ','|_|','  |'],
        '5':[' _ ','|_ ',' _|'],
        '6':[' _ ','|_ ','|_|'], 
        '7':['_ ',' |',' |'], 
        '8':[' _ ','|_|','|_|'],
        '9':[' _ ','|_|',' _|'], 
        '0':[' _ ','| |','|_|']
   }

   def __init__(self,number):
      self.number = number

   def toString(self):
      result = ['\n','\n','\n']
      for digit in str(self.number):
         code = self.DIGITS[digit]
         for index,digitString in enumerate(code):
            result[index] = result[index][:-1] + digitString + result[index][-1:]
         
      return result[0] + result[1] + result[2]

