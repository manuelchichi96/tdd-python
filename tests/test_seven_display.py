import pytest

from src.display.seven_display import *

def test_true():

   firstTest = SevenDisplay(1234567890)
   firstWord =   '  _  _     _  _ _  _  _  _ \n| _| _||_||_ |_  ||_||_|| |\n||_  _|  | _||_| ||_| _||_|\n'
                  
   secondTest = SevenDisplay(1978)
   secondWord =  '  _ _  _ \n||_| ||_|\n| _| ||_|\n'
   
   thirdTest = SevenDisplay(450)
   thirdWord =  '    _  _ \n|_||_ | |\n  | _||_|\n'
                  
   assert firstTest.toString() == firstWord
   assert secondTest.toString() == secondWord
   assert thirdTest.toString() == thirdWord

  
   
